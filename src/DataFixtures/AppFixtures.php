<?php

namespace App\DataFixtures;

use App\Entity\Blog;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $faker = Factory::create('fr_FR');

        for ($i = 0; $i < 10; $i++) {
            $blog = new Blog();
            $blog->setTitre($faker->words(3, true))
                ->setTexte($faker->text(250))
                ->setSlug($faker->slug(3))
                ->setCreateDate($faker->dateTimeBetween('-6 month', 'now'))
                ->setPhoto($faker->image(null, 640, 480));

            $manager->persist($blog);
        }
        $manager->flush();
    }
}
