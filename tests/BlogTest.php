<?php

namespace App\Tests;

use App\Entity\Blog;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Validator\Constraints\Date;

class BlogTest extends TestCase
{
    public function testIsTrue(): void
    {
        $blog = new Blog();
      $date =   new \DateTime();
        $blog->setTitre("Titre")
        ->setCreateDate($date)
            ->setPhoto('photo.png')
            ->setSlug("Slug")
            ->setTexte("Text");
        $this->assertTrue($blog->getTitre() === 'Titre');
        $this->assertTrue($blog->getPhoto() === 'photo.png');
        $this->assertTrue($blog->getSlug() === 'Slug');
        $this->assertTrue($blog->getTexte() === 'Text');
        $this->assertTrue($blog->getCreateDate() === $date);
    }

    public function testIsFalse(): void
    {
        $blog = new Blog();
        $date =   new \DateTime();
        $blog->setTitre("TrueTitre")
            ->setCreateDate($date)
            ->setPhoto('photo.png')
            ->setSlug("Slug")
            ->setTexte("Text");
        $this->assertFalse($blog->getTitre() === 'FalseTitre');
        $this->assertFalse($blog->getPhoto() === 'Falsephoto.png');
        $this->assertFalse($blog->getSlug() === 'FalseSlug');
        $this->assertFalse($blog->getTexte() === 'FalseText');
        $this->assertFalse($blog->getCreateDate() === 'true');
    }

    public function testIsEmpty(): void
    {
        $blog = new Blog();

        $this->assertEmpty($blog->getTitre());
        $this->assertEmpty($blog->getPhoto());
        $this->assertEmpty($blog->getSlug());
        $this->assertEmpty($blog->getTexte());
        $this->assertEmpty($blog->getCreateDate());
    }
}
