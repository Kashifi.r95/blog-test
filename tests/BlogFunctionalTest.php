<?php

namespace App\Tests;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class BlogFunctionalTest extends WebTestCase
{
    public function testSomething(): void
    {
        $client = self::createClient();
        $crawler = $client->request('GET', '/');

       $this->assertTrue($client->getResponse()->isSuccessful());
       $this->assertSame(1, $crawler->filter('html:contains("Hello World")')->count());
       $this->assertSelectorTextContains('a', 'Hello World');




    }
}
